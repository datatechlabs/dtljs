/**
 * Created by Aivis on 19/02/2017.
 */

import React from 'react';
import Checkbox from './../forms/checkbox';
import Button from './../forms/button';
import TextField from './../forms/textfield'
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'
import Select from 'react-select';
var cx = require('classnames');
var imm = require('immutable');
var RandExp = require('randexp');

class TableHead extends React.Component {
    render() {
        return (
            <thead>
            <tr>
                {
                    this.props.columns.map(function(cols, i) {
                        if (cols.visible === false) {
                            return;
                        }
                        return <th key={i}>{cols.label}</th>
                    })
                }
            </tr>
            </thead>
        );
    }
}

class ButtonDelete extends React.Component {
    handleDelete(data) {
        if(confirm("Are you sure?")) {
            var d = new $.Deferred();
            if(this.props.pk == undefined) {
                return d.reject('error message'); //returning error via deferred object
            } else {
                var opt = {
                    url: this.props.url + '/' + this.props.pk,
                    urlattrs: this.props.urlattrs,
                    api_base: this.props.api_base_url,
                    authToken: getCookie(this.props.token_name),
                    method: 'delete'
                };
                var data = {
                    pk: this.props.pk
                };

                var that = this;
                $.when( execAjax(data, opt) ).done(function() {
                    that.props.onDelete(data);
                    toastr.options.timeOut = 5000;
                    toastr.options.positionClass = "toast-top-center";
                    toastr.options.closeButton = true;
                    toastr['info']("Success");
                    return d.resolve();
                }).fail(function(result) {
                    toastr.options.timeOut = 5000;
                    toastr.options.positionClass = "toast-top-center";
                    toastr.options.closeButton = true;
                    toastr['error'](result.msg);
                    return d.reject(result.msg);
                });
                return d.promise();
            }
        }
    }
    render() {
        return (
                <Button bsStyle='danger' onClick={this.handleDelete.bind(this)} value="Delete" />
        )
    }
}

class ButtonEdit extends React.Component {
    handleEdit(data) {
        this.props.onEdit(data);
    }
    render() {
        return (
                <Button bsStyle='warning' onClick={this.handleEdit.bind(this)} value="Edit" />
        )
    }
}

class ButtonSave extends React.Component {
    handleSave(data) {
        this.props.onSave(data);
    }
    render() {
        return (
            <Button bsStyle='info' onClick={this.handleSave.bind(this)} value="Save" />
        )
    }
}

class ButtonCancel extends React.Component {
    handleCancel(data) {
        this.props.onCancel(data);
    }
    render() {
        return (
            <Button bsStyle='default' onClick={this.handleCancel.bind(this)} value="Cancel" />
        )
    }
}

class TableRow extends React.Component {
    constructor (props) {
        super(props)
        var initdata = imm.Map();
        this.props.metadata.map(function (col, key) {
            initdata = initdata.set(col.source, this.props.row[col.source]);
        }, this);
        this.state = {
            edit: false,
            data: initdata,
            backupdata: initdata,
            errormsg: ''
        }
    }
    componentDidMount() {

    }
    componentDidUpdate() {
        var flag = false;
        if (this.state.errormsg != '') {
            if (this.state.errormsg.fields != undefined) {
                {
                    this.state.errormsg.fields.map(function (row, index) {
                        {
                            this.props.metadata.map(function (mrow, mindex) {
                                if (mrow.source == row) {
                                    toastr.options.timeOut = 5000;
                                    toastr.options.positionClass = "toast-top-center";
                                    toastr.options.closeButton = true;
                                    toastr['error']("Error in field: " + mrow.label);
                                    this.setState({errormsg: ''});
                                    flag = true;
                                }
                            }, this)
                        };
                    }, this)
                }
            };
            if (flag == false) {
                toastr.options.timeOut = 5000;
                toastr.options.positionClass = "toast-top-center";
                toastr.options.closeButton = true;
                toastr['error'](this.state.errormsg);
                this.setState({errormsg: ''});
            }
        }
    }
    onUpdate(val){
        this.props.onUpdate(val);
    }
    onDelete(val) {
        this.props.onDelete(val);
    }
    onEdit(val) {
        this.setState({edit: true});
    }
    onSave(val) {
        this.setState({errorfields: [], errormsg: ''});
        var data = this.state.data.toJS();
        var opt = {
            url: this.props.url + '/' + this.props.id,
            urlattrs: this.props.urlattrs,
            method: 'update',
            api_base: this.props.api_base_url,
            authToken: getCookie('sessid_admin')
        };
        var that = this;
        $.when( execAjax(data, opt) ).done(function(data) {
            that.setState({
                data: imm.Map(data),
                backupdata: imm.Map(data),
                edit: false
            });

            toastr.options.timeOut = 5000;
            toastr.options.positionClass = "toast-top-center";
            toastr.options.closeButton = true;
            toastr['success']('Entry Updated Successfully');

        }).fail(function(result) {
            if (result != undefined) {
                if (result.fields == undefined) {
                    var flds = [];
                } else {
                    var flds = result.fields;
                }
                that.setState({
                    errorfields: flds,
                    errormsg: result.msg
                });
            }
        });
    }
    onCancel(val) {
        this.setState({
            edit: false,
            data: this.state.backupdata
        });
    }
    handleChange(key, value) {
        this.setState((state) => ({
            data: state.data.set(key, value)
        }));
    }
    handleSelectChange(key, value) {
        var val = value.value;
        this.setState((state) => ({
            data: state.data.set(key, val)
        }));
    }
    render() {
        var pk = this.props.id;
        return (
            <tr key={pk}>
                {this.props.metadata.map(function(col, index) {

                    if (col.type == undefined) {
                        var ctype = col.type_edit;
                    } else {
                        var ctype = col.type;
                    }

                    if (ctype == 'primary') {
                        pk = this.props.row[col.source];
                    }
                    var meta = {};
                    if (this.props.remotemeta == undefined) {
                        meta = {};
                    } else {
                        meta = this.props.remotemeta[col.source];
                        if (meta == undefined) {
                            meta = {};
                        }
                    }
                    // buttons
                    if (ctype == 'action_buttons') {
                        if (!this.state.edit) {
                            return (
                                <td key={index}>
                                    <ButtonEdit
                                        index={index}
                                        pk={pk}
                                        onEdit={this.onEdit.bind(this)}/>
                                    <ButtonDelete
                                        index={index}
                                        pk={pk}
                                        url={this.props.url}
                                        urlattrs={this.props.urlattrs}
                                        api_base_url={this.props.api_base_url}
                                        token_name={this.props.token_name}
                                        onDelete={this.onDelete.bind(this)}/>
                                </td>)
                        } else {
                            return (
                                <td key={index}>
                                    <ButtonSave
                                        index={index}
                                        pk={pk}
                                        url={this.props.url}
                                        urlattrs={this.props.urlattrs}
                                        api_base={this.props.api_base}
                                        token_name={this.props.token_name}
                                        onSave={this.onSave.bind(this)}/>
                                    <ButtonCancel
                                        index={index}
                                        pk={pk}
                                        token_name={this.props.token_name}
                                        onCancel={this.onCancel.bind(this)}/>
                                </td>)
                        }
                    }
                    // invisible columns
                    if (col.visible === false) {
                        return;
                    }
                    // SELECTs
                    if (ctype == 'select') {
                        if (!this.state.edit || !col.editable) {
                            var key = this.state.data.get(col.source);
                            return <td key={index}>{meta.values[key]}</td>
                        } else {
                            //TODO: fix valuesif cases
                            var options = [];
                            Object.keys(meta.values).map(function(i) {
                                options.push({value: i, label: meta.values[i]});
                            }, this);
                            return  (
                                <td key={index}>
                                    <Select
                                        name={col.source}
                                        options={options}
                                        selected={this.props.row[col.source]}
                                        id={col.source}
                                        placeholder={col.label}

                                        value={ this.state.data.get(col.source) }
                                        onChange={this.handleSelectChange.bind(this, col.source)}
                                    />
                                </td>
                            )
                        }
                    }
                    // MULTISELECTS
                    if (ctype == 'multiple') {
                        if (!this.state.edit || !col.editable) {
                            var options = [];
                            Object.keys(meta.values).map(function(i) {
                                options.push({value: i, label: meta.values[i]});
                            }, this);
                            return  (
                                <td key={index}>
                                    <Select
                                        name={col.source}
                                        options={options}
                                        selected={this.props.row[col.source]}
                                        id={col.source}
                                        multi
                                        simpleValue
                                        disabled
                                        placeholder={col.label}
                                        value={ this.state.data.get(col.source) }
                                        onChange={this.handleChange.bind(this, col.source)}
                                    />
                                </td>
                            )
                        } else {
                            //TODO: fix valuesif cases
                            var options = [];
                            Object.keys(meta.values).map(function(i) {
                                options.push({value: i, label: meta.values[i]});
                            }, this);
                            return  (
                                <td key={index}>
                                    <Select
                                        name={col.source}
                                        options={options}
                                        selected={this.props.row[col.source]}
                                        id={col.source}
                                        multi
                                        simpleValue
                                        placeholder={col.label}
                                        value={ this.state.data.get(col.source) }
                                        onChange={this.handleChange.bind(this, col.source)}
                                    />
                                </td>
                            )
                        }
                    }
                    // CHECKBOXes
                    if (ctype == 'checkbox') {
                        if (!this.state.edit || !col.editable) {
                            return(
                                <td key={ index }>
                                    <Checkbox
                                        label=''
                                        checked={ this.state.data.get(col.source) }
                                        placeholder={ col.label }
                                        className="form-control"
                                        disabled = {true}
                                    />
                                </td>
                            )
                        } else {
                            return (
                                <td key={ index }>
                                    <Checkbox
                                        label=''
                                        checked={ this.state.data.get(col.source) }
                                        placeholder={ col.label }
                                        onChange={ this.handleChange.bind(this, col.source) }
                                        className="form-control"
                                        disabled = {false}
                                    />
                                </td>
                            )
                        }
                    }
                    // URLs (not editable AFAIK
                    if (ctype == 'link') {
                        var url = col.value;
                        url = url.replace('{$id}', pk);
                        return <td key={ index }><Link to={url} className="btn btn-view">{col.label}</Link></td>
                    }
                    // Password fields
                    if (ctype == 'password') {
                        if (!this.state.edit || !col.editable) {
                            return <td key={index}>********</td>
                        } else {
                            return (
                                <td key={index}>
                                    <TextField
                                        label=''
                                        type='text'
                                        value={ this.state.data.get(col.source) }
                                        placeholder={ col.label }
                                        onChange={ this.handleChange.bind(this, col.source) }
                                        className="form-control"
                                    /></td>)
                        }
                    }
                    // text fields
                    if (ctype == 'text') {
                        if (!this.state.edit || !col.editable) {
                            return <td key={index}>{this.state.data.get(col.source)}</td>
                        } else {
                            return (
                                <td key={index}>
                                    <TextField
                                        label=''
                                        type='text'
                                        value={ this.state.data.get(col.source) }
                                        placeholder={ col.label }
                                        onChange={ this.handleChange.bind(this, col.source) }
                                        className="form-control"
                                    /></td>)
                        }
                    }
                }, this)}
            </tr>
        );
    }

}

class TableBody extends React.Component {
    constructor () {
        super()
        this.state = {
            data: {data: []},
            metadata: [],
            idcol: null
        }
    }

    onRemove(val){
        var newdata = {};
        newdata.data = [];
        this.state.data.data.map(function(nrow, i) {
            if (nrow[this.state.idcol] != val.pk) {
                newdata.data[i] = nrow;
            }
        }, this);
        this.setState({
            data: newdata
        });
    }
    
    componentWillReceiveProps(nextProps) {
        this.setState({
            data: nextProps.data
        })

    }

    componentDidMount() {
        var idcol = null;
        this.props.metadata.map(function(col, index) {

            if (col.type == 'primary') {
                idcol = col.source;
            }
        }, this);

        this.setState({
            idcol: idcol
        })
    }

    render() {
        return (
            <div>
                <table className={ cx(this.props.className || 'table table-bordered table-striped table-editable') }  >
                    <TableHead columns={this.props.metadata}/>
                    <tbody>
                    {
                        this.state.data.data.map(function(datarow, i) {

                            var id = datarow[this.state.idcol];
                            return (
                                <TableRow
                                    key={i}
                                    id={id}
                                    row={datarow}
                                    metadata={this.props.metadata}
                                    remotemeta = {this.props.data.metadata}
                                    url = {this.props.url}
                                    urlattrs={this.props.urlattrs}
                                    api_base_url={this.props.api_base_url}
                                    token_name = {this.props.token_name}
                                    onDelete={this.onRemove}
                                />
                            )
                        }, this)
                    }
                    </tbody>
                </table>
            </div>
        );
    }
};

class EdiTable2 extends React.Component {
    render(){
        return (
            <div>
                <div className="row">
                    <div className="col-lg-12">

                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <TableBody
                            data={this.props.data}
                            metadata={this.props.metadata}
                            url={this.props.url}
                            urlattrs={this.props.urlattrs}
                            api_base_url={this.props.api_base_url}
                            token_name = {this.props.token_name}
                            className={ this.props.className }
                        />
                    </div>
                </div>
            </div>
        );
    }
};

module.exports = EdiTable2;