import getCookie from './getCookie';

function getSitePrefix(cookie) {
    var cvalue = getCookie(cookie);
    if (cvalue == undefined) {
        return '';
    }
    if (cvalue == '') {
        return '';
    }

    return '/' + cvalue;
}

module.exports = getSitePrefix;
