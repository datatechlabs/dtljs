
function execAjax(data, opt, redirect) {
    var d = new $.Deferred();
    var method = opt.method;

    var authToken = opt.authToken;
    var url = opt.url;
    var urlattrs = opt.urlattrs;
    var api_base = opt.api_base;

    if (urlattrs != undefined) {
        for (var key in urlattrs) {
            var repl = '{$' + key + '}';
            url = url.replace(repl, urlattrs[key]);
        }
    }

    var filter = opt.filter;
    var fstring = '';
    if (filter != undefined) {
        for (var key in filter) {
            fstring = fstring + '&' + key + '=' + filter[key];
        }
    }

    if (method == 'get') {
        var type = 'GET';
        var qs = '';
        for (var key in data) {
            qs += '&' + key + '=' + data[key];
        }
        var url = api_base + '/' + url + '?method=' + method + fstring +'&authToken=' + authToken + qs;
        var data = {};
        var contType = "";
    } else {
        var type = 'POST';
        var url = api_base + '/' + url + '?method=' + method;
        var contType = "application/x-www-form-urlencoded; charset=UTF-8";
        data.authToken = authToken;
    }

    $.ajax({
        url: url,
        type: type,
        data: data,
        contentType: contType,
        success: function success(data) {
            if (data.result == undefined && method == 'get') {
                d.resolve(data);
            } else if (data.result == "0") {
                d.resolve(data.data);
            } else {
                if (data.msg == null) {
                    d.reject({ msg: "An error has occured" });
                } else {
                    var resp = { msg: data.msg, fields: data.fields };
                    d.reject(resp);
                }
            }
        },
        error: function error(xhr, status, err) {
            if (xhr.status == 401) {
                var resp = { msg: 'Unauthorized' };
                if (redirect != undefined) window.location = redirect;
            } else if (xhr.status == 404) {
                var resp = { msg: 'Not Found' };
            } else {
                if (xhr.responseJSON != undefined) {
                    var resp = { msg: xhr.responseJSON };
                } else {
                    var resp = { msg: xhr.statusText };
                }
            }
            d.reject(resp);
        }
    });
    return d.promise();
}

module.exports = execAjax;