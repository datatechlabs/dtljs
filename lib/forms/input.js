const React = require('react');

class Input extends React.Component {
    handleChange(e) {
        this.props.onChange(e.target.value);
    }
    render() {
        const { error, block } = this.props;
        const label = this.props.label || '';

        // use label itself as ID (sanitized)
        let { id } = this.props;
        if (!id && typeof label == 'string')
            id = 'id_textfield_'+label.replace(/[^A-Za-z0-9-_]/g, '');

        // convert undefined and null values to an empty string
        var value = this.props.value;
        if (typeof value != 'string' && typeof value != 'number')
            value = '';

        return (
                <input
                    autoComplete={ this.props.autoComplete }
                    className={ this.props.className }
                    disabled={ this.props.disabled }
                    onChange={ this.handleChange.bind(this) }
                    placeholder={ this.props.placeholder }
                    type={ this.props.type || 'text' }
                    value={ value }
                    id={ id }
                />
        );
    }
};

module.exports = Input;
