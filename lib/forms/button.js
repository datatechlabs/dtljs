/**
 * Created by Aivis on 21/11/2016.
 */

import React from 'react';
var classNames = require('classnames');

class Button extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        this.props.onClick(this);
    }

    render() {
        if (this.props.bsStyle != undefined) {
            var style = 'btn btn-' + this.props.bsStyle;
        } else {
            var style = 'btn btn-default';
        }
        return (
            <button disabled={this.props.disabled} className={style} type="button" onClick={this.handleClick.bind(this)}>{this.props.value}</button>
        );
    }
}

module.exports = Button;