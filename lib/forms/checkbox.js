const React = require('react');
const cx = require('classnames');
const Toggle = require('react-toggle').default;

class Checkbox extends React.Component {
    handleChange(e) {
        this.props.onChange(e.target.checked);
    }
    render() {
        const { block, error } = this.props;

        // use label itself as ID (sanitized)
        const label = this.props.label || '';
        const disabled = this.props.disabled || false;
        const id = this.props.id || 'id_checkbox_'+label.replace(/[^A-Za-z0-9-_]/g, '');

        // convert undefined and null values to an empty string
        var checked = this.props.checked;
        if (typeof checked != 'string' && typeof checked != 'boolean')
            checked = false;
        if (typeof checked == 'string' && checked == '1')
            checked = true;
        if (typeof checked == 'string' && checked == '0')
            checked = false;

        return (
            <div className={ cx('form-group', { 'has-error': !!error }) }>
                <label className={ cx('control-label', {'col-sm-3': !block}) } htmlFor={ id }>{ label }</label>
                <div className={ cx('checkbox', {'col-sm-9':!block}) }>
                    <Toggle
                        id={ id }
                        checked={ checked }
                        onChange={ this.handleChange.bind(this) }
                        disabled = { disabled }
                    />
                    { error
                        ? <span className='help-block'>{ error }</span>
                        : null }
                </div>
            </div>
        );
    }
};

module.exports = Checkbox;