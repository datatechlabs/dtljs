const React = require('react');
const cx = require('classnames');

class TextArea extends React.Component {
    handleChange(e) {
        this.props.onChange(e.target.value);
    }
    render() {
        const { error, block } = this.props;
        const label = this.props.label || '';

        // use label itself as ID (sanitized)
        let { id } = this.props;
        if (!id && typeof label == 'string')
            id = 'id_textfield_'+label.replace(/[^A-Za-z0-9-_]/g, '');

        // convert undefined and null values to an empty string
        var value = this.props.value;
        if (typeof value != 'string' && typeof value != 'number')
            value = '';

        return (
            <div className={ cx('form-group', { 'has-error': !!error }) }>
                { label ?
                    <label htmlFor={ id } className={ cx('control-label', {'col-sm-3': !block}) }>
                        { label }
                    </label> : null }
                <textarea
                    autoComplete={ this.props.autoComplete }
                    className={ this.props.className }
                    disabled={ this.props.disabled }
                    onChange={ this.handleChange.bind(this) }
                    placeholder={ this.props.placeholder }
                    type={ this.props.type || 'text' }
                    value={ value }
                    id={ id }
                    rows = { this.props.rows }
                />
                { error
                    ? <span className='help-block'>{ error }</span>
                    : null }
            </div>
        );
    }
};

module.exports = TextArea;