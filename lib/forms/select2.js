/**
 * Created by Aivis on 21/11/2016.
 */

import React from 'react';
var classNames = require('classnames');

class Select2 extends React.Component {
    componentDidMount() {
        this.renderSelect2();
    }
    renderSelect2() {
        $('.select2_' + this.props.id).select2({
            multiple: 'multiple'
        });
    }
    render() {
        if (this.props.values == undefined)
            return null;
        var idclass = "select2_" + this.props.id;
        var parentClass = this.props.className;
        var selectClasses = classNames({
            [`select2_${this.props.id}`]: true,
            [`${this.props.className}`]: true
        });
        return (
            <select name={this.props.name} defaultValue={this.props.selected} className={selectClasses} multiple="true" onChange={this.props.onChange.bind(this)}>
                {
                    Object.keys(this.props.values).map(function(i) {
                        return (<option key={i} value={i}>{this.props.values[i]}</option>);
                    }, this)
                }
            </select>
        );
    }
};

module.exports = Select2;