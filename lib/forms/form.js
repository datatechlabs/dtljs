const cx = require('classnames');
const React = require('react');

class Form extends React.Component {
    handleSubmit(e) {
        e.preventDefault();
        this.props.onSubmit();
    }
    render() {
        var error = this.props.error;
        var submitting = this.props.submitting;
        var greyed = this.props.greyed;
        var info = this.props.info;
        return (
            <form className={ cx(this.props.className, { 'vdom-loading': submitting }) }>
                { error
                    ? <div className='alert alert-danger'>
                    { error }
                </div>
                    : null }
                { info
                    ? <div className='alert alert-info'>
                    { info }
                </div>
                    : null }
                { this.props.children }
                <button
                    type='submit'
                    onClick={ this.handleSubmit.bind(this) }
                    className={ cx(this.props.submitButtonClass || 'btn-block btn-lg btn-primary',
        { disabled: submitting || greyed }) }
                    disabled={ submitting || greyed }
                >
                    { this.props.submitButtonText || 'Submit' }
                </button>
            </form>
        );
    }
};

module.exports = Form;
