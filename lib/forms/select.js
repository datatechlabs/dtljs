/**
 * Created by Aivis on 21/11/2016.
 */


import React from 'react';
var classNames = require('classnames');

class Select extends React.Component {
    handleChange(e) {
        this.props.onChange(e.target.value);
    }
    render() {
        if (this.props.options == undefined) {
            return null;
        }
        return (
            <select name={this.props.name} defaultValue={this.props.selected} className={this.props.className} onChange={this.handleChange.bind(this)}>
                {this.props.options.map(function(row, i) {
                        return (
                            <option key={i} value={row.value}>{row.label}</option>
                    )
                }, this)}
            </select>
        );
    }
};

module.exports = Select;